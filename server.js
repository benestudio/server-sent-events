const express = require('express')
const app = express()
const port = 3001;
const streams = require('./streams');
const cors = require('cors');

app.use(cors());

app.get('/sse/:username', async (req, res) => {
    // TODO complete the response header with 2-3 more properties. Research what you need to make it to an event source.
    res.writeHead(200, {
        'Access-Control-Allow-Origin': '*',
    });
    // Add res and the username to the streams in an object
    res.write(sse('Connected to the Event source'))

    req.on('close', () => {
        // TODO remove stream from streams
    })
});

const sse = (message) => {
    return `data: ${ message }\n\n`
}

app.listen(port, () => console.log(`Example app listening on port ${port}!`))